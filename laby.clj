; Voici notre labyrinthe !
(def transitions {
  :1  '(:2)
  :2  '(:1 :7)
  :3  '(:6)
  :4  '(:5)
  :5  '(:4 :12)
  :6  '(:3 :7)
  :7  '(:2 :6 :8)
  :8  '(:7 :9)
  :9  '(:8 :10)
  :10 '(:9 :11 :15)
  :11 '(:10 :12 :14)
  :12 '(:5 :11)
  :13 '(:20)
  :14 '(:11)
  :15 '(:10 :16)
  :16 '(:15 :17)
  :17 '(:16 :18)
  :18 '(:17 :19)
  :19 '(:18 :20)
  :20 '(:13 :19)})

(def laby (hash-map
  :depart '(:1)  
  :arrivee '(:20)
  :transitions transitions))

(defn nextStates [m t] (get (get m :transitions) t))

(defn explore' [laby vus file]
  (if (= (length file) 0)
    nil
    (let [
      element (first file)
      pretendants (get (get laby :transitions) element)
      nouveaux (filter fn[x](not (contains? vus x)) pretendants)
      nouvFile (concat (rest file) nouveaux)
      nouvVus (cons element vus)
          ]
